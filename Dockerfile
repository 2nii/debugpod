FROM python:latest

WORKDIR /usr/src/app

RUN apt-get update && apt-get install -y apt-utils # bash already installed 

RUN apt-get update && \ 
apt-get install -y gnupg software-properties-common curl && \
curl -fsSL https://apt.releases.hashicorp.com/gpg | apt-key add - && \
apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main" && \
apt-get update && apt-get install terraform


COPY build/requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

RUN python -m pip install --upgrade pip
RUN python -m pip install --user ansible

COPY build/* ./    

CMD [ "python", "./freeze.py" ]
